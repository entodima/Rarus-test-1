<?php
include('src/BracketsStringScanner.php');

$input = $argv[1];
if (!isset($input)) exit ('Строка не задана!'."\n");

$result = BracketsStringScanner::scan($input);

if (!$result) echo("🛑 Количество открывающихся и закрывающихся скобок не совпадает!\n") ;
else echo "✅ Количество открывающихся и закрывающихся скобок совпадает.\n";