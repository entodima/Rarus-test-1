# Задача №1 — проверка расстановки скобок


[Библиотека на packagist.org](https://packagist.org/packages/entodima/rarustest1)

Статус сборки теста: [![Build Status](https://travis-ci.com/entodima/Rarus-test-1.svg?branch=master)](https://travis-ci.com/entodima/Rarus-test-1)

<hr>

[Задание](https://github.com/rarus/intern-php-developer/blob/master/task01-brackets.md)

