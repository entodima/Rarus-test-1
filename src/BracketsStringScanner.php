<?php

class BracketsStringScanner {
    public static function scan($string){
        $left = $right = $brackets = 0; //brackets - количество пар скобок
        for ($i=0; $i<strlen($string); $i++){
            $symbol = $string[$i];
            if ($symbol=='(') $left++;
            if ($symbol ==')'){
                if ($left>0){
                    $left--;
                    $brackets++;
                } else $right++;
            }
        }
        if ($left>0 or $right>0) return false;
        else return true;
    }
}