<?php
require_once 'src/BracketsStringScanner.php';

use PHPUnit\Framework\TestCase;

class BracketsStringScannerTest extends TestCase
{
    public function testScanTrue()
    {
        $true_string = '5 * (4 - 2)';
        $result_string = BracketsStringScanner::scan($true_string);
        $this->assertEquals(true,$result_string);
    }

    public function testScanFalse()
    {
        $false_string = '5 * (4 - 2(';
        $result_string = BracketsStringScanner::scan($false_string);
        $this->assertEquals(false,$result_string);
    }
}
